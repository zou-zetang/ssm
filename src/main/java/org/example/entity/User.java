/**
 * {@code @Title} User
 * {@code @Author}  wang
 * {@code @Date}  2023/9/7 14:03
 * {@code @Copyright} Copyright (c) 2023/9/7
 */
package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String userName;
    private String passWord;

}
