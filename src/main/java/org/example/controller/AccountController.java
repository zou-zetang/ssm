/**
 * {@code @Title} AccountController
 * {@code @Author}  wang
 * {@code @Date}  2023/9/7 14:01
 * {@code @Copyright} Copyright (c) 2023/9/7
 */
package org.example.controller;

import org.springframework.stereotype.Controller;

@Controller
public class AccountController {
    //注册
    public String addUser(){
        return "";
    }
    //登录
    public String login(){
        return "";
    }
}
